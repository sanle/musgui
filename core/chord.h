#ifndef CHORD_H
#define CHORD_H

#include <list>

#include "note_position.h"

namespace Chord
{
	std::list<note_position> getMajorChord(note_position base, bool iunversion=false);	
	std::list<note_position> getMinorChord(note_position base, bool inversion=false);	
	std::list<note_position> getBigMajorSeventhChord(note_position base, bool inversion = false);
	std::list<note_position> getSmallMajorSeventhChord(note_position base, bool inversion = false);
	std::list<note_position> getBigMinorSeventhChord(note_position base, bool inversion = false);
	std::list<note_position> getSmallMinorSeventhChord(note_position base, bool inversion = false);
}

#endif
