#include <list>
#include <wx/wx.h>
#include "./core/note_position.h"

class Staff: public wxPanel
{
	public:
		Staff(wxFrame *parent);
		void setNoteList(std::list<note_position> inList);
		void clearNoteList();
		void addToEndNoteList(std::list<note_position> inList);
		void resetData();
		void setClef(int clef);
	protected:
		void onPaint(wxPaintEvent& event);
		void onScroll(wxScrollWinEvent& event);
		void onScrolled(wxScrollWinEvent& event);
		void onSize(wxSizeEvent& event);
	private:
		const int noteStartX = 65;
		const int staffStartY = 59;
		const int noteStepX =30;
		const int baseStartDrawingX = 10;
		const int endLineX = 1200;
		const int staffStepY=150;
		const int tenorClefCorrector =10;
		const int gClefCorrector = 5;
		wxSize wSize;
		int currentClef =0;
		int staffNum=0;
		int endLineXCorrector = 0;
		int scrollCorrector = 0;
		void drawStaff(wxDC& dc,int startY, int clef = 0);
		void drawNote(wxDC& dc, int x, int y);
		void drawAccidental(wxDC& dc,char acc, int x, int y);
		void drawAdditionalLine(wxDC& dc,char col, int startX, int startY);
		void drawBarSeparator(wxDC& dc, int x);
		std::list<note_position> toDraw;

};
