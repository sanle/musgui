#include "../core/base_tones.h"
#include "../core/note_operations.h"
#include "catch.hpp"

TEST_CASE("Note Operations", "[note_operations]")
{
	SECTION("Operation with notes")
	{
		NoteOperations mc = NoteOperations();
		note_position np = g_H;
		note_position new_np;
		new_np = mc.reductionNote(np);
		REQUIRE(new_np.alt == -1);
		new_np = mc.reductionNote(new_np);
		REQUIRE(new_np.alt == -2);
		new_np = mc.reductionNote(new_np);
		REQUIRE(new_np.alt == -2);
		new_np.alt = 2;
		new_np = mc.reductionNote(new_np);
		REQUIRE(new_np.alt == 1);
		new_np = mc.reductionNote(new_np);
		REQUIRE(new_np.alt == 0);
		new_np = mc.increaseNote(new_np);
		REQUIRE(new_np.alt == 1);
		new_np = mc.increaseNote(new_np);
		REQUIRE(new_np.alt == 2);
		new_np = mc.increaseNote(new_np);
		REQUIRE(new_np.alt == 2);
		new_np.alt = -2;
		new_np = mc.increaseNote(new_np);
		REQUIRE(new_np.alt == -1);
		new_np = mc.increaseNote(new_np);
		REQUIRE(new_np.alt == 0);
		new_np = NoteOperations::getNotePosition(49);
		REQUIRE(np == new_np);
	}
	SECTION ("Intervals")
	{
		NoteOperations mc = NoteOperations();
		note_position np = g_H;
		note_position new_np;
		note_position wanted = {52,90,0,0,1,0,0,0,0,0};
		new_np = mc.getSmallSecunde(np,false);
		REQUIRE(new_np == wanted);
		wanted = {47,80,0,0,8,0,0,0,0,0};
		new_np = mc.getSmallSecunde(np);
		REQUIRE(new_np == wanted);
		wanted = {53,90,0,0,8,0,0,0,0,0};
		new_np = mc.getBigSecunde(np,false);
		REQUIRE(new_np == wanted);
		wanted = {46,80,0,0,1,0,0,0,0,0};
		new_np = mc.getBigSecunde(np);
		REQUIRE(new_np == wanted);
		wanted = {55,95,0,0,1,0,0,0,0,0};
		new_np = mc.getSmallTerz(np,false);
		REQUIRE(new_np == wanted);
		wanted = {44,75,0,0,8,0,0,0,0,0};
		new_np = mc.getSmallTerz(np);
		REQUIRE(new_np == wanted);
	}
	SECTION ("Transposing")
	{
		std::list<note_position>  l = {{17,30,2,35,8,0,0,0,0,0}};
		std::list<note_position>  tl = {{29,50,0,0,8,0,0,0,0,0}};
		std::list<note_position>  rl = {{17,30,2,35,8,0,0,0,0,0}};
		NoteOperations::convertToClef(rl,0,0);
		bool res = (l == rl);
		REQUIRE(res);
		NoteOperations::convertToClef(rl,1,0);
		res = (rl == tl); 
		REQUIRE(res);
		NoteOperations::convertToClef(rl,0,1);
		res = (rl == l); 
		REQUIRE(res);
		tl= {{35,60,0,0,8,0,0,0,0,0}};
		NoteOperations::convertToClef(rl,2,0);
		res = (rl == tl); 
		REQUIRE(res);
		NoteOperations::convertToClef(rl,0,2);
		res = (rl == l); 
		REQUIRE(res);
		tl= {{32,55,0,0,8,0,0,0,0,0}};
		NoteOperations::convertToClef(rl,3,0);
		res = (rl == tl); 
		REQUIRE(res);
		NoteOperations::convertToClef(rl,0,3);
		res = (rl == l); 
		REQUIRE(res);
		tl= {{23,40,1,45,8,0,0,0,0,0}};
		NoteOperations::convertToClef(rl,2,1);
		res = (rl == tl); 
		REQUIRE(res);
		NoteOperations::convertToClef(rl,1,2);
		res = (rl == l); 
		REQUIRE(res);
		tl= {{20,35,2,35,8,0,0,0,0,0}};
		NoteOperations::convertToClef(rl,3,1);
		res = (rl == tl); 
		REQUIRE(res);
		NoteOperations::convertToClef(rl,1,3);
		res = (rl == l); 
		REQUIRE(res);
		tl= {{14,25,3,25,8,0,0,0,0,0}};
		NoteOperations::convertToClef(rl,3,2);
		res = (rl == tl); 
		REQUIRE(res);
		NoteOperations::convertToClef(rl,2,3);
		res = (rl == l); 
		REQUIRE(res);
		tl= {{20,35,2,35,8,0,0,0,0,0}};
		rl= {{14,25,3,25,8,0,0,0,0,0}};
		NoteOperations::convertToClef(rl,0,3);
		res = (rl == tl); 
		REQUIRE(res);
		tl= {{68,115,-2,115,8,0,0,0,0,0}};
		rl= {{71,120,-2,115,8,0,0,0,0,0}};
		NoteOperations::convertToClef(rl,2,0);
		res = (rl == tl); 
		REQUIRE(res);
	}

}
