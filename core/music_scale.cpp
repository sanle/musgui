#include "music_scale.h"
#include <cstdlib>
#include <algorithm>
#include <iterator>
#include <stdexcept>
#include "base_tones.h"
#include "note_operations.h"

static const char chromatic_restricted_index[] ={6,7,18,19,27,28,39,40,48,49,60,61,69,70};

MusicScale::MusicScale()
{
	for(char i=0; i<note_col; i++)
	{
		scale.push_front(notes[i]);
	}
}

std::list<note_position> MusicScale::getChromatic() const
{
	std::list<note_position> chromatic_scale;
	std::copy_if(scale.begin(),scale.end(),std::back_inserter(chromatic_scale),
	[](note_position el){ 
		int * pItem=(int *)std::binary_search(std::begin(chromatic_restricted_index),std::end(chromatic_restricted_index),el.id);
		if(pItem!=nullptr)
		{
			return false;
		}
		return (el.alt>=0);
	});
	return chromatic_scale;
}

std::list<note_position> MusicScale::getDiatonic() const
{
	std::list<note_position> diatonic_scale;
	std::copy_if(scale.begin(),scale.end(),std::back_inserter(diatonic_scale),[](note_position el){ return (el.alt==8);});
	return diatonic_scale;
}

std::list<note_position> MusicScale::getMajorPentatonic(note_position base) const
{
	std::list<note_position> major_pentatonic;
	major_pentatonic.push_back(base);
	base=NoteOperations::getBigSecunde(base);
	major_pentatonic.push_back(base);
	base=NoteOperations::getBigSecunde(base);
	major_pentatonic.push_back(base);
	base=NoteOperations::getSmallTerz(base);
	major_pentatonic.push_back(base);
	base=NoteOperations::getBigSecunde(base);
	major_pentatonic.push_back(base);
	return major_pentatonic;
}

std::list<note_position> MusicScale::getMinorPentatonic(note_position base) const
{
	std::list<note_position> major_pentatonic;
	major_pentatonic.push_back(base);
	base=NoteOperations::getSmallTerz(base);
	major_pentatonic.push_back(base);
	base=NoteOperations::getBigSecunde(base);
	major_pentatonic.push_back(base);
	base=NoteOperations::getBigSecunde(base);
	major_pentatonic.push_back(base);
	base=NoteOperations::getSmallTerz(base);
	major_pentatonic.push_back(base);
	return major_pentatonic;
}

std::list<note_position> MusicScale::getNaturalMajor(note_position base, bool arp, char col) const
{
	std::list<note_position> natural_major;
	for (int i = 0; i < col; i++)
	{
		natural_major.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		if (!arp)
		{
			natural_major.push_back(base);
		}
		base=NoteOperations::getBigSecunde(base);
		natural_major.push_back(base);
		base=NoteOperations::getSmallSecunde(base);
		if (!arp)
		{
			natural_major.push_back(base);
		}
		base=NoteOperations::getBigSecunde(base);
		natural_major.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		if (!arp)
		{
			natural_major.push_back(base);
		}
		base=NoteOperations::getBigSecunde(base);
		if (!arp)
		{
			natural_major.push_back(base);
		}
		base=NoteOperations::getSmallSecunde(base);
	}
	natural_major.push_back(base);
	for (int i = 0; i < col; i++)
	{
		base=NoteOperations::getSmallSecunde(base,false);
		if (!arp)
		{
			natural_major.push_back(base);
		}
		base=NoteOperations::getBigSecunde(base,false);
		if (!arp)
		{
			natural_major.push_back(base);
		}
		base=NoteOperations::getBigSecunde(base,false);
		natural_major.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		if (!arp)
		{
			natural_major.push_back(base);
		}
		base=NoteOperations::getSmallSecunde(base,false);
		natural_major.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		if (!arp)
		{
			natural_major.push_back(base);
		}
		base=NoteOperations::getBigSecunde(base,false);
		natural_major.push_back(base);
	}
	return natural_major;
}

std::list<note_position> MusicScale::getHarmonicMajor(note_position base, char col) const
{
	std::list<note_position> harmonic_major;
	for (int i = 0; i < col; i++)
	{
		harmonic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		harmonic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		harmonic_major.push_back(base);
		base=NoteOperations::getSmallSecunde(base);
		harmonic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		harmonic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		base=NoteOperations::reductionNote(base);// NoteOperations::reduction six stage
		harmonic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		harmonic_major.push_back(base);
		base=NoteOperations::getSmallSecunde(base);
	}
	harmonic_major.push_back(base);
	for(int i = 0; i < col; i++)
	{
		base=NoteOperations::getSmallSecunde(base,false);
		harmonic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		base=NoteOperations::reductionNote(base);// NoteOperations::reduction six stage
		harmonic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		harmonic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		harmonic_major.push_back(base);
		base=NoteOperations::getSmallSecunde(base,false);
		harmonic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		harmonic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		harmonic_major.push_back(base);
	}
	return harmonic_major; 
}

std::list<note_position> MusicScale::getMelodicMajor(note_position base, char col) const
{
	std::list<note_position> melodic_major;
	for (int i = 0; i < col; i++)
	{
		melodic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		melodic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		melodic_major.push_back(base);
		base=NoteOperations::getSmallSecunde(base);
		melodic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		melodic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		melodic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		melodic_major.push_back(base);
		base=NoteOperations::getSmallSecunde(base);
	}
	melodic_major.push_back(base);
	for (int i = 0; i < col; i++)
	{
		base=NoteOperations::getSmallSecunde(base,false);
		base=NoteOperations::reductionNote(base);// NoteOperations::reduction seven stage
		melodic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		base=NoteOperations::reductionNote(base);// NoteOperations::reduction six stage
		melodic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		melodic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		melodic_major.push_back(base);
		base=NoteOperations::getSmallSecunde(base,false);
		melodic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		melodic_major.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		melodic_major.push_back(base);
	}
	return melodic_major;
}

std::list<note_position> MusicScale::getNaturalMinor(note_position base, bool arp, char col) const
{
	std::list<note_position> natural_minor;
	for (int i = 0; i < col; i++)
	{
		natural_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		if (!arp)
		{
			natural_minor.push_back(base);
		}
		base=NoteOperations::getSmallSecunde(base);
		natural_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		if (!arp)
		{
			natural_minor.push_back(base);
		}
		base=NoteOperations::getBigSecunde(base);
		natural_minor.push_back(base);
		base=NoteOperations::getSmallSecunde(base);
		if (!arp)
		{
			natural_minor.push_back(base);
		}
		base=NoteOperations::getBigSecunde(base);
		if (!arp)
		{
			natural_minor.push_back(base);
		}
		base=NoteOperations::getBigSecunde(base);
	}
	natural_minor.push_back(base);
	for ( int i = 0; i < col; i++)
	{
		base=NoteOperations::getBigSecunde(base,false);
		if (!arp)
		{
			natural_minor.push_back(base);
		}
		base=NoteOperations::getBigSecunde(base,false);
		if (!arp)
		{
			natural_minor.push_back(base);
		}
		base=NoteOperations::getSmallSecunde(base,false);
		natural_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		if (!arp)
		{
			natural_minor.push_back(base);
		}
		base=NoteOperations::getBigSecunde(base,false);
		natural_minor.push_back(base);
		base=NoteOperations::getSmallSecunde(base,false);
		if (!arp)
		{
			natural_minor.push_back(base);
		}
		base=NoteOperations::getBigSecunde(base,false);
		natural_minor.push_back(base);
	}
	return natural_minor;	
}

std::list<note_position> MusicScale::getHarmonicMinor(note_position base, char col) const
{
	std::list<note_position> harmonic_minor;
	for (int i = 0; i < col; i++)
	{
		harmonic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		harmonic_minor.push_back(base);
		base=NoteOperations::getSmallSecunde(base);
		harmonic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		harmonic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		harmonic_minor.push_back(base);
		base=NoteOperations::getSmallSecunde(base);
		harmonic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		base=NoteOperations::increaseNote(base); // NoteOperations::increase seven stage
		harmonic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base);
	}
	harmonic_minor.push_back(base);
	for (int i = 0; i < col; i++)
	{
		base=NoteOperations::getBigSecunde(base,false);
		base=NoteOperations::increaseNote(base); // NoteOperations::increase seven stage
		harmonic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		harmonic_minor.push_back(base);
		base=NoteOperations::getSmallSecunde(base,false);
		harmonic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		harmonic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		harmonic_minor.push_back(base);
		base=NoteOperations::getSmallSecunde(base,false);
		harmonic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		harmonic_minor.push_back(base);
	}
	return harmonic_minor;
}

std::list<note_position> MusicScale::getMelodicMinor(note_position base, char col) const
{
	std::list<note_position> melodic_minor;
	for (int i = 0; i < col; i++)
	{
		melodic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		melodic_minor.push_back(base);
		base=NoteOperations::getSmallSecunde(base);
		melodic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		melodic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		melodic_minor.push_back(base);
		base=NoteOperations::getSmallSecunde(base);
		base=NoteOperations::increaseNote(base); // NoteOperations::increase six stage
		melodic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base);
		base=NoteOperations::increaseNote(base); // NoteOperations::increase seven stage
		melodic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base);

	}
	melodic_minor.push_back(base);
	for ( int i = 0; i < col; i++)
	{
		base=NoteOperations::getBigSecunde(base,false);
		melodic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		melodic_minor.push_back(base);
		base=NoteOperations::getSmallSecunde(base,false);
		melodic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		melodic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		melodic_minor.push_back(base);
		base=NoteOperations::getSmallSecunde(base,false);
		melodic_minor.push_back(base);
		base=NoteOperations::getBigSecunde(base,false);
		melodic_minor.push_back(base);
	}
	return melodic_minor;
}

void MusicScale::setClef(int clef)
{
	currentClef = clef;
}

