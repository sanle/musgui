#ifndef TONALITY_CIRCLE_H
#define TONALITY_CIRCLE_H
	
#include "circle_list.h"
#include "tonality.h"

class TonalityCircle
{
	public:
		TonalityCircle();
		std::string getMajor() const;
		char getMajorIndex() const;
		std::string getMinor() const;
		char getMinorIndex() const;
		Tonality getElement() const;
		Tonality getOnenameMajor() const;//TODO
		std::string getOnenameMajorName() const;//TODO
		Tonality getOnenameMinor() const;//TODO
		std::string getOnenameMinorName() const;//TODO
		void rotateLeft();
		void rotateRight();
		char getIndexByMajorName(const std::string name);
		char getIndexByMinorName(const std::string name);
		CircularList<Tonality>::iterator getIterator();
	private:
		CircularList<Tonality> circle;
		Tonality element;	
};
#endif
