#ifndef NOTE_OPERATIONS_H
#define NOTE_OPERATIONS_H

#include <list>
#include "note_position.h"

extern note_position notes[72];

class NoteOperations
{
	public:
		static note_position getSmallSecunde(const note_position &base, bool up = true);
		static note_position getBigSecunde(const note_position &base, bool up = true);
		static note_position getSmallTerz(const note_position &base, bool up = true);
		static note_position getBigTerz(const note_position &base, bool up = true);
		static note_position reductionNote(note_position base);
		static note_position increaseNote(note_position base);
		static note_position getNotePosition(char index);
		static void convertToClef(std::list<note_position> &in, int clef, int prevClef);

	private: 
		static char calcStage(char base_id, char generate_id);
		static char getInterval(char base_id, char tone_sum, char stage_num, bool up);
		static const char note_col = 72;

};

#endif
