
#include "../core/tonality.h"
#include "catch.hpp"

TEST_CASE("Tonality class test", "[tonality]")
{
	SECTION("Default constructor")
	{
		Tonality ton = Tonality();
		REQUIRE (ton.getNameMajor() == "C");
		REQUIRE (ton.getNameMinor() == "a");
		REQUIRE (ton.getFullNameMajor() == "DO");
		REQUIRE (ton.getFullNameMinor() == "LA");
		REQUIRE (ton.getAlt() == 0);
		REQUIRE (ton.getColAlt() == 0);
		REQUIRE (ton.getMinorIndex() == 52);
		REQUIRE (ton.getMajorIndex() == 46);
	}
	SECTION("CONSTRUCTOR")
	{
		Tonality ton = Tonality("Ges","es","G flat major","e minor",-1,6,56,41);
		REQUIRE (ton.getNameMajor() == "Ges");
		REQUIRE (ton.getNameMinor() == "es");
		REQUIRE (ton.getFullNameMajor() == "G flat major");
		REQUIRE (ton.getFullNameMinor() == "e minor");
		REQUIRE (ton.getAlt() == -1);
		REQUIRE (ton.getColAlt() == 6);
		REQUIRE (ton.getMinorIndex() == 41);
		REQUIRE (ton.getMajorIndex() == 56);
	}
	SECTION("compare")
	{
		Tonality ton = Tonality("Ges","es","G flat major","e minor",-1,6,56,41);
		Tonality ton1 = Tonality();
		REQUIRE (ton != ton1);
	}
}
