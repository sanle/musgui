#include "chord.h"

#include "base_tones.h"
#include "note_operations.h"

std::list<note_position> Chord::getMajorChord(note_position base,bool inversion)
{
	std::list<note_position> chord;
	note_position third;
	note_position fifth;
	chord.push_back(CHORD_START);
	chord.push_back(base);
	third=NoteOperations::getBigTerz(base);
	chord.push_back(third);
	fifth=NoteOperations::getSmallTerz(third);
	chord.push_back(fifth);
	chord.push_back(CHORD_END);
	if(!inversion)
	{
		return chord;
	}
	if((base.id - OCTAVE_OFFSET -1 > 0) && (third.id - OCTAVE_OFFSET - 1  > 0))
	{
		base=NoteOperations::getNotePosition(base.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(third);
		chord.push_back(fifth);
		chord.push_back(base);
		chord.push_back(CHORD_END);
		third=NoteOperations::getNotePosition(third.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(fifth);
		chord.push_back(base);
		chord.push_back(third);
		chord.push_back(CHORD_END);
	}
		
	return chord;
}

std::list<note_position> Chord::getMinorChord(note_position base, bool inversion)
{
	std::list<note_position> chord;
	note_position third;
	note_position fifth;

	chord.push_back(CHORD_START);
	chord.push_back(base);
	third=NoteOperations::getSmallTerz(base);
	chord.push_back(third);
	fifth=NoteOperations::getBigTerz(third);
	chord.push_back(fifth);
	chord.push_back(CHORD_END);
	if(!inversion)
	{
		return chord;
	}
	if((base.id - OCTAVE_OFFSET -1 > 0) && (third.id - OCTAVE_OFFSET -1 > 0))
	{
		base=NoteOperations::getNotePosition(base.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(third);
		chord.push_back(fifth);
		chord.push_back(base);
		chord.push_back(CHORD_END);
		third=NoteOperations::getNotePosition(third.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(fifth);
		chord.push_back(base);
		chord.push_back(third);
		chord.push_back(CHORD_END);
	}
	return chord;
}

std::list<note_position> Chord::getBigMajorSeventhChord(note_position base, bool inversion)
{
	std::list<note_position> chord;
	note_position third;
	note_position fifth;
	note_position seventh;
	third = NoteOperations::getBigTerz(base);
	fifth = NoteOperations::getSmallTerz(third);
	seventh = NoteOperations::getSmallTerz(fifth);
	chord.push_back(CHORD_START);
	chord.push_back(base);
	chord.push_back(third);
	chord.push_back(fifth);
	chord.push_back(seventh);
	chord.push_back(CHORD_END);
	if(!inversion)
	{
		return chord;
	}
	if((base.id - OCTAVE_OFFSET -1 > 0) && (third.id - OCTAVE_OFFSET -1 > 0) && (fifth.id - OCTAVE_OFFSET -1 > 0))
	{
		base = NoteOperations::getNotePosition(base.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(third);
		chord.push_back(fifth);
		chord.push_back(seventh);
		chord.push_back(base);
		chord.push_back(CHORD_END);
		third = NoteOperations::getNotePosition(third.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(fifth);
		chord.push_back(seventh);
		chord.push_back(base);
		chord.push_back(third);
		chord.push_back(CHORD_END);
		fifth = NoteOperations::getNotePosition(fifth.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(seventh);
		chord.push_back(base);
		chord.push_back(third);
		chord.push_back(fifth);
		chord.push_back(CHORD_END);
	}
	return chord;
}

std::list<note_position> Chord::getSmallMajorSeventhChord(note_position base, bool inversion)
{
	std::list<note_position> chord;
	note_position third;
	note_position fifth;
	note_position seventh;
	third = NoteOperations::getBigTerz(base);
	fifth = NoteOperations::getSmallTerz(third);
	seventh = NoteOperations::getBigTerz(fifth);
	chord.push_back(CHORD_START);
	chord.push_back(base);
	chord.push_back(third);
	chord.push_back(fifth);
	chord.push_back(seventh);
	chord.push_back(CHORD_END);
	if(!inversion)
	{
		return chord;
	}
	if((base.id - OCTAVE_OFFSET -1 > 0) && (third.id - OCTAVE_OFFSET -1 > 0) && (fifth.id - OCTAVE_OFFSET -1 > 0))
	{
		base = NoteOperations::getNotePosition(base.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(third);
		chord.push_back(fifth);
		chord.push_back(seventh);
		chord.push_back(base);
		chord.push_back(CHORD_END);
		third = NoteOperations::getNotePosition(third.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(fifth);
		chord.push_back(seventh);
		chord.push_back(base);
		chord.push_back(third);
		chord.push_back(CHORD_END);
		fifth = NoteOperations::getNotePosition(fifth.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(seventh);
		chord.push_back(base);
		chord.push_back(third);
		chord.push_back(fifth);
		chord.push_back(CHORD_END);
	}
	return chord;
}

std::list<note_position> Chord::getBigMinorSeventhChord(note_position base, bool inversion)
{
	std::list<note_position> chord;
	note_position third;
	note_position fifth;
	note_position seventh;
	third = NoteOperations::getSmallTerz(base);
	fifth = NoteOperations::getBigTerz(third);
	seventh = NoteOperations::getBigTerz(fifth);
	chord.push_back(CHORD_START);
	chord.push_back(base);
	chord.push_back(third);
	chord.push_back(fifth);
	chord.push_back(seventh);
	chord.push_back(CHORD_END);
	if(!inversion)
	{
		return chord;
	}
	if((base.id - OCTAVE_OFFSET -1 > 0) && (third.id - OCTAVE_OFFSET -1 > 0) && (fifth.id - OCTAVE_OFFSET -1 > 0))
	{
		base = NoteOperations::getNotePosition(base.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(third);
		chord.push_back(fifth);
		chord.push_back(seventh);
		chord.push_back(base);
		chord.push_back(CHORD_END);
		third = NoteOperations::getNotePosition(third.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(fifth);
		chord.push_back(seventh);
		chord.push_back(base);
		chord.push_back(third);
		chord.push_back(CHORD_END);
		fifth = NoteOperations::getNotePosition(fifth.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(seventh);
		chord.push_back(base);
		chord.push_back(third);
		chord.push_back(fifth);
		chord.push_back(CHORD_END);
	}
	return chord;
}

std::list<note_position> Chord::getSmallMinorSeventhChord(note_position base, bool inversion)
{
	std::list<note_position> chord;
	note_position third;
	note_position fifth;
	note_position seventh;
	third = NoteOperations::getSmallTerz(base);
	fifth = NoteOperations::getBigTerz(third);
	seventh = NoteOperations::getSmallTerz(fifth);
	chord.push_back(CHORD_START);
	chord.push_back(base);
	chord.push_back(third);
	chord.push_back(fifth);
	chord.push_back(seventh);
	chord.push_back(CHORD_END);
	if(!inversion)
	{
		return chord;
	}
	if((base.id - OCTAVE_OFFSET -1 > 0) && (third.id - OCTAVE_OFFSET -1 > 0) && (fifth.id - OCTAVE_OFFSET -1 > 0))
	{
		base = NoteOperations::getNotePosition(base.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(third);
		chord.push_back(fifth);
		chord.push_back(seventh);
		chord.push_back(base);
		chord.push_back(CHORD_END);
		third = NoteOperations::getNotePosition(third.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(fifth);
		chord.push_back(seventh);
		chord.push_back(base);
		chord.push_back(third);
		chord.push_back(CHORD_END);
		fifth = NoteOperations::getNotePosition(fifth.id - OCTAVE_OFFSET-1);
		chord.push_back(CHORD_START);
		chord.push_back(seventh);
		chord.push_back(base);
		chord.push_back(third);
		chord.push_back(fifth);
		chord.push_back(CHORD_END);
	}
	return chord;
}
