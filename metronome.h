#ifndef METRONOME_H
#define METRONOME_H

#include <wx/spinctrl.h>
#include <wx/timer.h>
#include <wx/wx.h>

class MetronomePanel : public wxPanel
{
	public:
		MetronomePanel(wxFrame *parent);
		void onTimer(wxTimerEvent& event);
		void onSpin(wxSpinEvent& event);
		wxBoxSizer *mainSizer;
		wxBoxSizer *spinSizer;
	private:
		wxTimer timer;
		wxSpinCtrl *spinner;
}

#endif
