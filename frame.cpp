#include "frame.h"
#include <list>
#include "./core/base_tones.h"
#include "./core/chord.h"
#include "./core/circle_list.h"
#include "./core/note_operations.h"
#include "./core/music_scale.h"
#include "./core/note_position.h"
#include "./core/tonality_circle.h"

Frame::Frame(const wxString& title)
	: wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(1280, 720))
{
	std::list<note_position> ol;
	mainSizer = new wxBoxSizer(wxVERTICAL);
	menubar = new wxMenuBar;
	file = new wxMenu;
	file->Append(wxID_EXIT, wxT("&Quit"));
	menubar->Append(file, wxT("&File"));
	scale= new wxMenu;
	scale->AppendRadioItem(11,wxT("Chromatic"));
	scale->AppendRadioItem(12,wxT("Diatonic"));
	scale->AppendRadioItem(14,wxT("Pentatonic"));
	scale->AppendRadioItem(23,wxT("Scale (music)"));
	scale->AppendRadioItem(24,wxT("Arpeggio"));
	scale->AppendRadioItem(26,wxT("Chord"));
	menubar->Append(scale, wxT("&Scales"));
	clefs = new wxMenu;
	clefs->AppendRadioItem(19,wxT("F-Bass"));
	clefs->AppendRadioItem(20,wxT("C-Tenor"));
	clefs->AppendRadioItem(21,wxT("C-Alto"));
	clefs->AppendRadioItem(22,wxT("G"));
	menubar->Append(clefs,wxT("&Clef"));
	about_app = new wxMenu;
	about_app->Append(wxID_ABOUT,wxT("About"));
	menubar->Append(about_app,wxT("About"));
	SetMenuBar(menubar);
	selectionSizer = new wxBoxSizer(wxHORIZONTAL);
	cbScale= new wxComboBox(this,17,wxT(""),wxDefaultPosition,wxDefaultSize,0,NULL,wxTE_PROCESS_ENTER|wxTE_PROCESS_TAB);
	cbTon = new wxComboBox(this,16,wxT(""),wxDefaultPosition,wxDefaultSize,0,NULL,wxTE_PROCESS_ENTER|wxTE_PROCESS_TAB);
	cbType= new wxComboBox(this,25,wxT(""),wxDefaultPosition,wxDefaultSize,0,NULL,wxTE_PROCESS_ENTER|wxTE_PROCESS_TAB);
	initComboBoxes();
	wxButton * choiseOk = new wxButton(this,18,wxT("Build"));
	selectionSizer->Add(cbScale,0,wxEXPAND|wxALL,0);
	selectionSizer->Add(cbTon,0,wxEXPAND|wxALL,0);
	selectionSizer->Add(cbType,0,wxEXPAND|wxALL,0);
	selectionSizer->Add(choiseOk,0,wxEXPAND|wxALL,0);
	staff= new Staff(this);
	mainSizer->Add(selectionSizer,0,wxEXPAND|wxALL,0);
	mainSizer->Hide(selectionSizer);
	mainSizer->Add(staff,1,wxEXPAND|wxALL,0);
	this->SetSizer(mainSizer);
	
	MusicScale mc;
	ol=mc.getChromatic();
	staff->setNoteList(ol);
	ol.pop_back();
	ol.reverse();
	staff->addToEndNoteList(ol);
	
	Connect(wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED,	wxCommandEventHandler(Frame::OnQuit));
	Connect(11,wxEVT_COMMAND_MENU_SELECTED,wxMenuEventHandler(Frame::rMenu));
	Connect(12,wxEVT_COMMAND_MENU_SELECTED,wxMenuEventHandler(Frame::rMenu));
	Connect(14,wxEVT_COMMAND_MENU_SELECTED,wxMenuEventHandler(Frame::rMenu));
	Connect(23,wxEVT_COMMAND_MENU_SELECTED,wxMenuEventHandler(Frame::rMenu));
	Connect(24,wxEVT_COMMAND_MENU_SELECTED,wxMenuEventHandler(Frame::rMenu));
	Connect(26,wxEVT_COMMAND_MENU_SELECTED,wxMenuEventHandler(Frame::rMenu));
	Connect(17,wxEVT_COMBOBOX,wxCommandEventHandler(Frame::changedScale));
	Connect(wxID_ABOUT,wxEVT_COMMAND_MENU_SELECTED,wxMenuEventHandler(Frame::aMenu));
	Connect(18,wxEVT_BUTTON,wxCommandEventHandler(Frame::buildScale));
	Connect(19,wxEVT_COMMAND_MENU_SELECTED,wxMenuEventHandler(Frame::clefMenu));
	Connect(20,wxEVT_COMMAND_MENU_SELECTED,wxMenuEventHandler(Frame::clefMenu));
	Connect(21,wxEVT_COMMAND_MENU_SELECTED,wxMenuEventHandler(Frame::clefMenu));
	Connect(22,wxEVT_COMMAND_MENU_SELECTED,wxMenuEventHandler(Frame::clefMenu));
	Centre();
}

void Frame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
	Close(true);
}

void Frame::rMenu(wxMenuEvent& event)
{
	MusicScale mc;
	std::list<note_position> lst;
	switch(event.GetId())
	{
		case 12:
		{
			lst=mc.getDiatonic();
			staff->setNoteList(lst);
			lst.pop_back();
			lst.reverse();
			staff->addToEndNoteList(lst);
			mainSizer->Hide(selectionSizer);
			break;
		}
		case 11:
		{
			lst=mc.getChromatic();
			staff->setNoteList(lst);
			lst.pop_back();
			lst.reverse();
			staff->addToEndNoteList(lst);
			mainSizer->Hide(selectionSizer);
			break;
		}
		case 14:
		{
			
			staff->clearNoteList();
			mainSizer->Show(selectionSizer);
			cbType->SetSelection(0);
			cbType->Hide();
			mainSizer->Layout();
			break;
		}
		case 23:
		{
			staff->clearNoteList();
			mainSizer->Show(selectionSizer);
			mainSizer->Layout();
			break;
		}
		case 24:
		{
			staff->clearNoteList();
			mainSizer->Show(selectionSizer);
			cbType->SetSelection(0);
			cbType->Hide();
			mainSizer->Layout();
			break;
		}
		case 26:
		{
			staff->clearNoteList();
			mainSizer->Show(selectionSizer);
			cbType->SetSelection(0);
			cbType->Hide();
			mainSizer->Layout();
			break;
		}
	}
	staff->resetData();
	staff->Refresh();
}

void Frame::aMenu(wxMenuEvent& event)
{
	wxMessageDialog *aMessage = new  wxMessageDialog(NULL,wxT("The application has developed by  SanLe.\n"),wxT("About program"),wxOK|wxCENTER|wxICON_INFORMATION);
	aMessage->ShowModal();
}

void Frame::initComboBoxes()
{
	wxString *st = new wxString();
	*st = "Major";
	cbScale->Append(1,st);
	*st = "Minor";
	cbScale->Append(1,st);
	*st = "Natural";
	cbType->Append(1,st);
	*st = "Harmonic";
	cbType->Append(1,st);
	*st = "Melodic";
	cbType->Append(1,st);

	cbScale->SetSelection(0);
	cbType->SetSelection(0);

	TonalityCircle *tc= new TonalityCircle();
	CircularList<Tonality>::iterator it(tc->getIterator());
	CircularList<Tonality>::iterator start(tc->getIterator());
	int i=2;
	*st = wxString::FromUTF8((*it).getFullNameMajor().c_str());
	cbTon->Append(1,st);
	--it;
	while(it!=start)
	{
		wxString *st = new wxString(wxString::FromUTF8((*it).getFullNameMajor().c_str()));
		cbTon->Append(1,st);
		--it;
		++i;
		delete(st);
	}

	cbTon->SetSelection(9);

	delete (st);
	delete (tc);
}

void Frame::changedScale(wxCommandEvent& event)
{
	wxString *st = new wxString();
	TonalityCircle *tc= new TonalityCircle();
	CircularList<Tonality>::iterator it(tc->getIterator());
	CircularList<Tonality>::iterator start(tc->getIterator());
	int i=2;
	cbTon->Clear();
	if(cbScale->GetSelection())
	{
		*st =wxString::FromUTF8((*it).getFullNameMinor().c_str());
		cbTon->Append(1,st);
		--it;
		while(it!=start)
		{
			wxString *st = new wxString(wxString::FromUTF8((*it).getFullNameMinor().c_str()));
			cbTon->Append(1,st);
			--it;
			++i;
			delete(st);
		}
	}
	else
	{
		*st = wxString::FromUTF8((*it).getFullNameMajor().c_str());
		cbTon->Append(1,st);
		--it;
		while(it!=start)
		{
			wxString *st = new wxString(wxString::FromUTF8((*it).getFullNameMajor().c_str()));
			cbTon->Append(1,st);
			--it;
			++i;
			delete(st);
		}
	}
	cbTon->SetSelection(9);
	delete (st);
	delete (tc);
}

void Frame::buildScale(wxCommandEvent& event)
{
	char index;
	bool scaleBuildingType;
	MusicScale mc;
	std::list<note_position> lst;
	TonalityCircle *tc= new TonalityCircle();
	if(menubar->IsChecked(14))
	{
		if(cbScale->GetSelection())
		{
			index=tc->getIndexByMinorName(cbTon->GetStringSelection().utf8_str().data());
			lst = mc.getMinorPentatonic(NoteOperations::getNotePosition(index));
		}
		else
		{
			index=tc->getIndexByMajorName(cbTon->GetStringSelection().utf8_str().data());
			lst = mc.getMajorPentatonic(NoteOperations::getNotePosition(index));
		}
		NoteOperations::convertToClef(lst,currentClef,0);
		staff->setNoteList(lst);
		staff->resetData();
		staff->Refresh();
		delete (tc);
		return;
	}
	if(menubar->IsChecked(26))
	{
		if(cbScale->GetSelection())
		{
			index=tc->getIndexByMinorName(cbTon->GetStringSelection().utf8_str().data());
			lst = Chord::getMinorChord(NoteOperations::getNotePosition(index));
		}
		else
		{
			index=tc->getIndexByMajorName(cbTon->GetStringSelection().utf8_str().data());
			lst = Chord::getMajorChord(NoteOperations::getNotePosition(index));
		}
		NoteOperations::convertToClef(lst,currentClef,0);
		staff->setNoteList(lst);
		staff->resetData();
		staff->Refresh();
		delete (tc);
		return;

	}
	scaleBuildingType=menubar->IsChecked(24);
	if(cbScale->GetSelection())
	{
		index=tc->getIndexByMinorName(cbTon->GetStringSelection().utf8_str().data());
		switch (cbType->GetSelection())
		{
			case 0:
			{
				lst = mc.getNaturalMinor(NoteOperations::getNotePosition(index),scaleBuildingType);
				NoteOperations::convertToClef(lst,currentClef,0);
				staff->setNoteList(lst);
				break;
			}
			case 1:
			{
				lst = mc.getHarmonicMinor(NoteOperations::getNotePosition(index));
				NoteOperations::convertToClef(lst,currentClef,0);
				staff->setNoteList(lst);
				break;
			}
			case 2:
			{
				lst = mc.getMelodicMinor(NoteOperations::getNotePosition(index));
				NoteOperations::convertToClef(lst,currentClef,0);
				staff->setNoteList(lst);

				break;
			}
		}

	}
	else
	{
		index=tc->getIndexByMajorName(cbTon->GetStringSelection().utf8_str().data());
		switch (cbType->GetSelection())
		{
			case 0:
			{
				lst = mc.getNaturalMajor(NoteOperations::getNotePosition(index), scaleBuildingType);
				NoteOperations::convertToClef(lst,currentClef,0);
				staff->setNoteList(lst);
				break;
			}
			case 1:
			{
				lst = mc.getHarmonicMajor(NoteOperations::getNotePosition(index));
				NoteOperations::convertToClef(lst,currentClef,0);
				staff->setNoteList(lst);
				break;
			}
			case 2:
			{
				lst = mc.getMelodicMajor(NoteOperations::getNotePosition(index));
				NoteOperations::convertToClef(lst,currentClef,0);
				staff->setNoteList(lst);
				break;
			}
		}
	}
	staff->resetData();
	staff->Refresh();
	delete (tc);
}

void Frame::clefMenu(wxMenuEvent& event)
{
	MusicScale mc;
	switch(event.GetId())
	{
		case 19:
		{
			staff->setClef(0);
			currentClef = 0;
			break;
		}
		case 20:
		{
			staff->setClef(1);
			currentClef = 1;
			break;
		}
		case 21:
		{
			staff->setClef(2);
			currentClef = 2;
			break;
		}
		case 22:
		{
			staff->setClef(3);
			currentClef = 3;
			break;
		}
	}
	staff->resetData();
	staff->Refresh();
}

