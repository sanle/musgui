#include <wx/combobox.h>
#include <wx/menu.h>
#include <wx/wx.h>
#include "staff.h"

class Frame : public wxFrame
{
	public:
		Frame(const wxString& title);
		void OnQuit(wxCommandEvent& event);
		void rMenu(wxMenuEvent& event);
		void aMenu(wxMenuEvent& event);
		void clefMenu(wxMenuEvent& event);
		void changedScale(wxCommandEvent& event);
		void buildScale(wxCommandEvent& event);
		wxMenuBar *menubar;
		wxMenu *file;
		wxMenu *scale;
		wxMenu *about_app;
		wxMenu *clefs;
		wxComboBox *cbTon;
		wxComboBox *cbScale;
		wxComboBox *cbType;
		wxBoxSizer *mainSizer;
		wxBoxSizer *selectionSizer;
		Staff *staff;
	private:
		void initComboBoxes();
		int currentClef = 0;
};

