CXX=g++

CXXFLAGS= $(shell wx-config --cxxflags  --debug=no) -std=c++11
LDFLAGS= $(shell wx-config --libs --debug=no)

CONF ?=DEBUG

ifeq ($(CONF),DEBUG)
   OUTDIR := build/debug
   CXXFLAGS= $(shell wx-config --cxxflags  --debug=yes) -std=c++11 -g
   LDFLAGS= $(shell wx-config --libs --debug=yes)
else
   ifeq ($(CONF),RELEASE)
     OUTDIR := build/release
   else
     OUTDIR :=test
     CXXFLAGS += --coverage
     LDFLAGS += --coverage
     .DEFAULT_GOAL := tests
   endif
endif

all: $(OUTDIR)/mus
	cp -r img $(OUTDIR)

$(addprefix $(OUTDIR)/,main.o staff.o frame.o music_scale.o tonality_circle.o tonality.o note_operations.o chord.o): $(OUTDIR)

$(OUTDIR):
	mkdir -p $(OUTDIR)

$(OUTDIR)/mus: $(addprefix $(OUTDIR)/,main.o staff.o frame.o music_scale.o tonality_circle.o tonality.o note_operations.o chord.o)
	$(CXX) -o $@ $^ $(LDFLAGS)

$(OUTDIR)/main.o: main.cpp main.h frame.h
	$(CXX) $(CXXFLAGS) -o $@ -c main.cpp 

$(OUTDIR)/frame.o: frame.cpp frame.h staff.h ./core/music_scale.h ./core/note_position.h ./core/base_tones.h ./core/tonality_circle.h ./core/note_operations.h
	$(CXX) $(CXXFLAGS) -o $@ -c $<

$(OUTDIR)/staff.o: staff.cpp staff.h ./core/note_position.h ./core/note_operations.h
	$(CXX) $(CXXFLAGS) -o $@ -c $<

$(OUTDIR)/chord.o: ./core/chord.cpp ./core/chord.h ./core/note_position.h ./core/note_operations.h ./core/base_tones.h
	$(CXX) $(CXXFLAGS) -o $@ -c $<

$(OUTDIR)/note_operations.o: ./core/note_operations.cpp ./core/note_operations.h ./core/note_position.h
	$(CXX) $(CXXFLAGS) -o $@ -c $<

$(OUTDIR)/music_scale.o: ./core/music_scale.cpp ./core/music_scale.h ./core/note_position.h ./core/note_operations.h
	$(CXX) $(CXXFLAGS) -o $@ -c $<

$(OUTDIR)/tonality_circle.o: ./core/tonality_circle.cpp ./core/tonality_circle.h ./core/tonality.h ./core/circle_list.h
	$(CXX) $(CXXFLAGS) -o $@ -c $<

$(OUTDIR)/tonality.o: ./core/tonality.cpp ./core/tonality.h
	$(CXX) $(CXXFLAGS) -o $@ -c $<

clean:
	rm  -rf $(OUTDIR)
clean-test:
	rm -f test/*.o
	rm -f test/*.gcno
	rm -f test/*.gcda
	rm -f test/tests

tests: $(addprefix $(OUTDIR)/,circle_list.t.o tonality.t.o tonality_circle.t.o music_scale.t.o note_position.t.o note_operations.t.o chord.t.o tonality.o music_scale.o tonality_circle.o note_operations.o chord.o)
	$(CXX) $^ $(LDFLAGS) -o  $(OUTDIR)/tests

$(OUTDIR)/circle_list.t.o: test/circle_list.t.cpp core/circle_list.h
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(OUTDIR)/tonality.t.o: test/tonality.t.cpp core/tonality.h core/tonality.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@
$(OUTDIR)/tonality_circle.t.o: test/tonality_circle.t.cpp core/tonality_circle.h core/tonality_circle.cpp core/tonality.h
	$(CXX) $(CXXFLAGS) -c $< -o $@
$(OUTDIR)/note_operations.t.o: test/note_operations.t.cpp core/note_operations.h core/note_operations.cpp core/note_position.h core/base_tones.h 
	$(CXX) $(CXXFLAGS) -c $< -o $@
$(OUTDIR)/music_scale.t.o: test/music_scale.t.cpp core/music_scale.h core/music_scale.cpp core/note_position.h core/base_tones.h core/note_operations.h
	$(CXX) $(CXXFLAGS) -c $< -o $@
$(OUTDIR)/note_position.t.o: test/note_position.t.cpp core/note_position.h core/base_tones.h
	$(CXX) $(CXXFLAGS) -c $< -o $@
$(OUTDIR)/chord.t.o: test/chord.t.cpp core/chord.h core/note_position.h core/note_operations.h core/base_tones.h
	$(CXX) $(CXXFLAGS) -o $@ -c $<
