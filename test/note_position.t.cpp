
#include "../core/base_tones.h"
#include "../core/note_position.h"
#include "catch.hpp"

TEST_CASE ("Note position","[note_position]")
{
	note_position np = l_C;
	note_position test_np = l_C;
	REQUIRE (np == test_np);
	test_np.id = 3;
	REQUIRE_FALSE(np == test_np);
	test_np = l_C;
	test_np.alt = -2;
	REQUIRE_FALSE(np == test_np);
	test_np = l_C;
	test_np.value = 4;
	REQUIRE_FALSE(np == test_np);
	test_np = l_D;
	REQUIRE_FALSE(np == test_np);
}
