#ifndef TONALITY_H
#define TONALITY_H

#include <string>

class Tonality
{
	private:
		std::string nameMajor;
		std::string nameMinor;
		std::string fullNameMajor;
		std::string fullNameMinor;
		char alt;
		char col_alt;
		char majorIndex;
		char minorIndex;
	public:
		Tonality();
		Tonality(const std::string& NameMajor,const std::string& NameMinor, const std::string& FullNameMajor, const std::string& FullNameMinor,char Alt, char Col, char Imajor, char Iminor);
		std::string getNameMajor() const;
		std::string getNameMinor() const;
		std::string getFullNameMajor() const;
		std::string getFullNameMinor() const;
		char getAlt() const;
		char getColAlt() const;
		char getMajorIndex() const;
		char getMinorIndex() const;
		bool operator!=(const Tonality &rhs) const;
};

struct tonality_struct
{
	std::string nameMajor;
	std::string nameMinor;
	std::string fullNameMajor;
	std::string fullNameMinor;
	char alt;
	char col_alt;
	char majorIndex;//indexes for array note_position in music_scale.cpp
	char minorIndex;
};
#endif
