#ifndef BASE_TONES_H
#define BASE_TONES_H

#define OCTAVE_OFFSET  21

// constant values and indexes for bass clef

#define BAR_SEPARATOR {-1,0,0,0,0,0,0,0,0,0,0}
#define CHORD_START {-2,0,0,0,0,0,0,0,0,0}
#define CHORD_END {-3,0,0,0,0,0,0,0,0,0}


// H great octave
#define g_H  {50,85,0,0,8,0,0,0,0,0}
#define g_H_index  49

//C low octave
#define l_C  {47,80,0,0,8,0,0,0,0,0}
#define l_C_index 46

//D low octave
#define l_D {44,75,0,0,8,0,0,0,0,0}
#define l_D_index 43

//E low octave
#define l_E {41,70,0,0,8,0,0,0,0,0}
#define l_E_index 40

//F low octave
#define l_F {38,65,0,0,8,0,0,0,0,0}
#define l_F_index 37

//G low octave 
#define l_G {35,60,0,0,8,0,0,0,0,0}
#define l_G_index 34

//A low octave 
#define l_A {32,55,0,0,8,0,0,0,0,0}
#define l_A_index 31

#endif
