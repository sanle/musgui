#include "staff.h"
#include <algorithm>
#include <iostream>
#include <iterator>
#include <wx/image.h>
#include <wx/scrolbar.h>
#include "./core/note_operations.h"

Staff::Staff(wxFrame *parent)
	: wxPanel(parent, wxID_ANY, wxDefaultPosition,wxDefaultSize, wxBORDER_NONE|wxVSCROLL)
{
	wxInitAllImageHandlers();
	SetScrollbar(wxVERTICAL,0,0,-1);
	AlwaysShowScrollbars(false,false);
	Connect(wxEVT_PAINT,wxPaintEventHandler(Staff::onPaint));
	Connect(wxEVT_SCROLLWIN_THUMBRELEASE,wxScrollWinEventHandler(Staff::onScroll));
	Connect(wxEVT_SCROLLWIN_THUMBTRACK,wxScrollWinEventHandler(Staff::onScrolled));
	Connect(wxEVT_SCROLLWIN_LINEUP,wxScrollWinEventHandler(Staff::onScrolled));
	Connect(wxEVT_SCROLLWIN_LINEDOWN,wxScrollWinEventHandler(Staff::onScrolled));
	Connect(wxEVT_SCROLLWIN_PAGEUP,wxScrollWinEventHandler(Staff::onScrolled));
	Connect(wxEVT_SCROLLWIN_PAGEDOWN,wxScrollWinEventHandler(Staff::onScrolled));
	Connect(wxEVT_SIZE,wxSizeEventHandler(Staff::onSize));
}

void Staff::onSize(wxSizeEvent& event)
{
	int cx,cy;
	wxSize currentSize = GetClientSize();
	endLineXCorrector = endLineX - currentSize.GetWidth();
	resetData();
	GetClientSize(&cx,&cy);
	Refresh();
}

void Staff::onScroll(wxScrollWinEvent& event)
{
	SetScrollPos(wxVERTICAL,event.GetPosition());
}

void Staff::onScrolled(wxScrollWinEvent& event)
{
	if(event.GetEventType() == wxEVT_SCROLLWIN_THUMBTRACK)
	{
		scrollCorrector=10*event.GetPosition();
	}
	if(event.GetEventType() == wxEVT_SCROLLWIN_LINEUP)
	{
		SetScrollPos(wxVERTICAL,GetScrollPos(wxVERTICAL)-1);
		scrollCorrector=10*(GetScrollPos(wxVERTICAL)-1);
	}
	if(event.GetEventType() == wxEVT_SCROLLWIN_LINEDOWN)
	{
		SetScrollPos(wxVERTICAL,GetScrollPos(wxVERTICAL)+1);
		scrollCorrector=10*(GetScrollPos(wxVERTICAL)+1);
	}
	if(event.GetEventType() == wxEVT_SCROLLWIN_PAGEUP)
	{
		SetScrollPos(wxVERTICAL,GetScrollPos(wxVERTICAL)-3);
		scrollCorrector=10*(GetScrollPos(wxVERTICAL)-3);
	}
	if(event.GetEventType() == wxEVT_SCROLLWIN_PAGEDOWN)
	{
		SetScrollPos(wxVERTICAL,GetScrollPos(wxVERTICAL)+3);
		scrollCorrector=10*(GetScrollPos(wxVERTICAL)+3);
	}
	staffNum=0;
	Refresh();
}

void Staff::onPaint(wxPaintEvent& event)
{
	wxPaintDC dc(this);
	bool chord = false;
	std::list<note_position>::iterator it;
	int x = noteStartX;
	int cx,cy;
	drawStaff(dc,staffStartY-scrollCorrector,currentClef);
	if(!toDraw.empty())
	{
		for(it = toDraw.begin();it!=toDraw.cend();++it)
		{
			switch(it->id)
			{
				case -3:
					chord = false;
					break;
				case -2:
					chord = true;
					break;
				case -1:
					drawBarSeparator(dc,x);
					break;
				default:
					drawNote(dc,x,it->y+(staffStepY*staffNum)-scrollCorrector);
			}
			if(it->id >0)
			{
				drawAdditionalLine(dc,it->additive_line,x,it->line_y+(staffStepY*staffNum)-scrollCorrector);
				drawAccidental(dc,it->alt,x-10,it->y+(staffStepY*staffNum)-scrollCorrector);
			}
			if(!chord)
			{
				x+=noteStepX;
			}
			if(x>=(endLineX-endLineXCorrector-20))
			{
				staffNum++;
				x=noteStartX;
				drawStaff(dc,staffStartY+(staffStepY*staffNum)-scrollCorrector,currentClef);
			}
		}	
		GetClientSize(&cx,&cy);
		if (cy < staffStartY+(staffStepY*staffNum))
		{
			int sp=GetScrollPos(wxVERTICAL);
			SetScrollbar(wxVERTICAL,sp,10,55);
		}
		else
		{
			SetScrollbar(wxVERTICAL,0,0,-1);
		}
	}
}

void Staff::drawStaff(wxDC& dc, int startY,int clef)
{
	wxBitmap current_clef;
	dc.SetPen(wxPen(*wxBLACK,1,wxSOLID));
	for(int i=0;i<5;i++)
	{
		dc.DrawLine(baseStartDrawingX,startY+i*10,endLineX-endLineXCorrector,startY+i*10);
	}
	switch (clef)
	{
		case 0:
		{
			current_clef.LoadFile(wxT("./img/bass_clef.png"),wxBITMAP_TYPE_PNG);
			if(current_clef.Ok())
			{
				dc.DrawBitmap(current_clef,baseStartDrawingX,startY,true);
			}
			break;
		}
		case 1:
		{
			current_clef.LoadFile(wxT("./img/c_clef.png"),wxBITMAP_TYPE_PNG);
			if(current_clef.Ok())
			{
				dc.DrawBitmap(current_clef,baseStartDrawingX,startY-tenorClefCorrector,true);
			}
			break;
		}
		case 2:
		{
			current_clef.LoadFile(wxT("./img/c_clef.png"),wxBITMAP_TYPE_PNG);
			if(current_clef.Ok())
			{
				dc.DrawBitmap(current_clef,baseStartDrawingX,startY,true);
			}
			break;
		}
		case 3:
		{
			current_clef.LoadFile(wxT("./img/g_clef.png"),wxBITMAP_TYPE_PNG);
			if(current_clef.Ok())
			{
				dc.DrawBitmap(current_clef,baseStartDrawingX,startY-gClefCorrector,true);
			}
			break;
		}
	}
}

void Staff::drawNote(wxDC& dc, int x, int y)
{
	wxBitmap w_note(wxT("./img/w_note.png"),wxBITMAP_TYPE_PNG);
	dc.DrawBitmap(w_note,x,y,true);

}

void Staff::drawAdditionalLine(wxDC& dc,char col, int startX, int startY)
{
	for(char i=0;i<abs(col);i++)
	{
		dc.SetPen(wxPen(*wxBLACK,1,wxSOLID));
		dc.DrawLine(startX-2,startY+4,startX+17,startY+4);
		if(col>0)
		{
			startY+=10;
		}
		else
		{
			startY-=10;
		}
	}
}

void Staff::drawAccidental(wxDC& dc, char acc, int x, int y)
{
	wxBitmap b_acc;
	switch(acc)
	{
		case -2:
		{
			b_acc.LoadFile(wxT("./img/double_flat.png"),wxBITMAP_TYPE_PNG);
			break;
		}
		case -1:
		{
			b_acc.LoadFile(wxT("./img/flat.png"),wxBITMAP_TYPE_PNG);
			break;
		}
		case 0:
		{
			b_acc.LoadFile(wxT("./img/natural.png"),wxBITMAP_TYPE_PNG);
			break;
		}
		case 1:
		{
			b_acc.LoadFile(wxT("./img/sharp.png"),wxBITMAP_TYPE_PNG);
			break;
		}
		case 2:
		{
			b_acc.LoadFile(wxT("./img/double_sharp.png"),wxBITMAP_TYPE_PNG);
			break;
		}
		default:
		{
			return;
		}
	}
	dc.DrawBitmap(b_acc,x,y,true);
}

void Staff::drawBarSeparator(wxDC& dc, int x)
{
	dc.SetPen(wxPen(*wxBLACK,2,wxSOLID));
	dc.DrawLine(x,staffStartY+(staffStepY*staffNum)-scrollCorrector,x,staffStartY+40+(staffStepY*staffNum)-scrollCorrector);

}

void Staff::setNoteList(std::list<note_position> inList)
{
	std::swap(toDraw,inList);
}

void Staff::resetData()
{
	staffNum=0;
	SetScrollPos(wxVERTICAL,0);
}

void Staff::setClef(int clef)
{
	NoteOperations::convertToClef(toDraw,clef,currentClef);
	currentClef=clef;
}

void Staff::clearNoteList()
{
	toDraw.clear();
}

void Staff::addToEndNoteList(std::list<note_position> inList)
{
	toDraw.splice(toDraw.end(),inList);
}
