#define CATCH_CONFIG_MAIN 

#include "../core/circle_list.h"
#include "catch.hpp"

TEST_CASE("Circular list check different operation" ,"[circular_list][circle_list]")
{

	CircularList<int> cl= CircularList<int>(1);
	SECTION("Rotation")
	{
		REQUIRE (cl.getCurrent() == 1);
		cl.RotateLeft();
		REQUIRE (cl.getCurrent() == 1);
		cl.RotateRight();
		REQUIRE (cl.getCurrent() == 1);
		cl.Add(2);
		REQUIRE (cl.getCurrent() == 2);
		cl.RotateLeft();
		REQUIRE (cl.getCurrent() == 1);
		cl.RotateRight();
		REQUIRE (cl.getCurrent() == 2);
	}
	SECTION("Add and delete element")
	{
		cl.Add(2);
		cl.deleteCurrent();
		REQUIRE (cl.getCurrent() == 1);
		cl.RotateLeft();
		REQUIRE (cl.getCurrent() == 1);
		cl.Add(3);
		REQUIRE (cl.getCurrent() == 3);
		cl.RotateLeft();
		REQUIRE (cl.getCurrent() == 1);
		cl.RotateRight();
		REQUIRE (cl.getCurrent() == 3);
	}
	SECTION("Empty list")
	{
		CircularList<int> el = CircularList<int>();
		REQUIRE_THROWS(el.getCurrent());
		el.Add(2);
		REQUIRE (el.getCurrent() == 2);
		el.deleteCurrent();
		REQUIRE_THROWS(el.getCurrent());
		el.Add(1);
		el.Add(2);
		el.RotateLeft();
		el.deleteCurrent();
		REQUIRE (el.getCurrent() == 2);
	}
	SECTION ("Iterators")
	{
		CircularList<int>::iterator it = CircularList<int>::iterator();
		it = cl.getFirst();
		REQUIRE ((*it) == 1);
		cl.Add(3);
		it = cl.getCurrentIterator();
		REQUIRE ((*it) == 3);
		it++;
		REQUIRE ((*it) == 1);
		--it;
		REQUIRE ((*it) == 3);
		++it;
		REQUIRE ((*it) == 1);
		it--;
		REQUIRE ((*it) == 3);
		REQUIRE ((*it) == 3);
		REQUIRE (it != cl.getFirst());
	}
	SECTION ("Copy constructor")
	{
		cl.Add(3);
		CircularList<int>::iterator it = CircularList<int>::iterator(cl.getFirst());
		CircularList<int> el = CircularList<int>(cl);
		REQUIRE (el.getCurrent() == (*it));
		el.Add(2);
		REQUIRE (el.getCurrent() != cl.getCurrent());
	}
}
