#include "tonality_circle.h"

static  tonality_struct  tonalities[]={{"es","as","до-бемоль","ля-бемоль",-1,7,47,53},
{"Ges","es","соль-бемоль","ми-бемоль",-1,6,56,41},
{"Des","b","ре-бемоль","си-бемоль",-1,5,44,50},
{"As","f","ля-бемоль","фа",-1,4,53,58},
{"Es","c","ми-бемоль","до",-1,3,41,46},
{"B","g","си-бемоль","соль",-1,2,50,55},
{"F","d","фа","ре",-1,1,58,43},
{"C","a","до","ля",0,0,46,52},
{"G","e","соль","ми",1,1,55,40},
{"D","h","ре","си",1,2,43,49},
{"A","fis","ля","фа-диез",1,3,52,57},
{"E","cis","ми","до-диез",1,4,40,45},
{"H","gis","си","соль-диез",1,5,49,53},
{"Fis","dis","фа-диез","ре-диез",1,6,57,42},
{"Cis","ais","до-диез","ля-диез",1,7,45,51}};

TonalityCircle::TonalityCircle()
{
	for(const tonality_struct &ts:tonalities)
	{
		circle.Add(Tonality(ts.nameMajor,ts.nameMinor,ts.fullNameMajor,ts.fullNameMinor,ts.alt,ts.col_alt,ts.majorIndex,ts.minorIndex));
	}
	element = circle.getCurrent();
}

std::string TonalityCircle::getMajor() const
{
	return element.getFullNameMajor();
}

char TonalityCircle::getMajorIndex() const
{
	return element.getMajorIndex();
}

std::string TonalityCircle::getMinor() const
{
	return element.getFullNameMinor();
}

char TonalityCircle::getMinorIndex() const
{
	return element.getMinorIndex();
}

void TonalityCircle::rotateLeft()
{
	circle.RotateLeft();
	element = circle.getCurrent();
}

void TonalityCircle::rotateRight()
{
	circle.RotateRight();
	element = circle.getCurrent();
}

Tonality TonalityCircle::getElement() const
{
	return element;
}

CircularList<Tonality>::iterator TonalityCircle::getIterator() 
{
	return circle.getCurrentIterator();
}

char TonalityCircle::getIndexByMajorName(const std::string name)
{
	CircularList<Tonality>::iterator it(getIterator());
	if(name == (*it).getFullNameMajor())
	{
		return (*it).getMajorIndex();
	}
	else
	{
		++it;
		CircularList<Tonality>::iterator start(getIterator());
		while(start!=it)
		{
			if(name == (*it).getFullNameMajor())
			{
				return (*it).getMajorIndex();
			}
			++it;
		}
		return -1;
	}
}


char TonalityCircle::getIndexByMinorName(const std::string name)
{
	CircularList<Tonality>::iterator it(getIterator());
	if(name == (*it).getFullNameMinor())
	{
		return (*it).getMinorIndex();
	}
	else
	{
		++it;
		CircularList<Tonality>::iterator start(getIterator());
		while(start!=it)
		{
			if(name == (*it).getFullNameMinor())
			{
				return (*it).getMinorIndex();
			}
			++it;
		}
		return -1;
	}
}
