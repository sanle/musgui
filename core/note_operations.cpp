#include "note_operations.h"
#include <cstdlib>
#include <stdexcept>
#include "base_tones.h"

note_position notes[72]={{1,5,5,5,1,0,0,0,0,0},{2,5,5,5,8,0,0,0,0,0},{3,5,5,5,-1,0,0,0,0,0},//D
		{4,10,4,15,1,0,0,0,0,0},{5,10,4,15,8,0,0,0,0,0},{6,10,4,15,-1,0,0,0,0,0},//C
		{7,15,4,15,1,0,0,0,0,0},{8,15,4,15,8,0,0,0,0,0},{9,15,4,15,-1,0,0,0,0,0},//B H
		{10,20,3,25,1,0,0,0,0,0,},{11,20,3,25,8,0,0,0,0,0},{12,20,3,25,-1,0,0,0,0,0},//A
		{13,25,3,25,1,0,0,0,0,0,},{14,25,3,25,8,0,0,0,0,0},{15,25,3,25,-1,0,0,0,0,0},//G
		{16,30,2,35,1,0,0,0,0,0,},{17,30,2,35,8,0,0,0,0,0},{18,30,2,35,-1,0,0,0,0,0},//F
		{19,35,2,35,1,0,0,0,0,0,},{20,35,2,35,8,0,0,0,0,0},{21,32,2,35,-1,0,0,0,0,0},//E
		{22,40,1,45,1,0,0,0,0,0,},{23,40,1,45,8,0,0,0,0,0},{24,40,1,45,-1,0,0,0,0,0},//D
		{25,45,1,45,1,0,0,0,0,0,},{26,45,1,45,8,0,0,0,0,0},{27,45,1,45,-1,0,0,0,0,0},//C
		{28,50,0,0,1,0,0,0,0,0},{29,50,0,0,8,0,0,0,0,0},{30,50,0,0,-1,0,0,0,0,0},//B H
		{31,55,0,0,1,0,0,0,0,0},{32,55,0,0,8,0,0,0,0,0},{33,55,0,0,-1,0,0,0,0,0},//A
		{34,60,0,0,1,0,0,0,0,0},{35,60,0,0,8,0,0,0,0,0},{36,60,0,0,-1,0,0,0,0,0},//G
		{37,65,0,0,1,0,0,0,0,0},{38,65,0,0,8,0,0,0,0,0},{39,65,0,0,-1,0,0,0,0,0},//F
		{40,70,0,0,1,0,0,0,0,0},{41,70,0,0,8,0,0,0,0,0},{42,70,0,0,-1,0,0,0,0,0},//E
		{43,75,0,0,1,0,0,0,0,0},{44,75,0,0,8,0,0,0,0,0},{45,75,0,0,-1,0,0,0,0,0},//D
		{46,80,0,0,1,0,0,0,0,0},{47,80,0,0,8,0,0,0,0,0},{48,80,0,0,-1,0,0,0,0,0},//C
		{49,85,0,0,1,0,0,0,0,0},{50,85,0,0,8,0,0,0,0,0},{51,85,0,0,-1,0,0,0,0,0},//B H
		{52,90,0,0,1,0,0,0,0,0},{53,90,0,0,8,0,0,0,0,0},{54,90,0,0,-1,0,0,0,0,0},//A
		{55,95,0,0,1,0,0,0,0,0},{56,95,0,0,8,0,0,0,0,0},{57,95,0,0,-1,0,0,0,0,0},//G
		{58,100,0,0,1,0,0,0,0,0},{59,100,0,0,8,0,0,0,0,0},{60,100,0,0,-1,0,0,0,0,0},//F
		{61,105,-1,105,1,0,0,0,0,0},{62,105,-1,105,8,0,0,0,0,0},{63,105,-1,105,-1,0,0,0,0,0},//E
		{64,110,-1,105,1,0,0,0,0,0},{65,110,-1,105,8,0,0,0,0,0},{66,110,-1,105,-1,0,0,0,0,0},//D
		{67,115,-2,115,1,0,0,0,0,0},{68,115,-2,115,8,0,0,0,0,0},{69,115,-2,115,-1,0,0,0,0,0},//C
		{70,120,-2,115,1,0,0,0,0,0},{71,120,-2,115,8,0,0,0,0,0},{72,120,-2,115,-1,0,0,0,0,0}//B H
};

static const char interval_index_down[]={0, 1, 1, 0, 1, 1, -1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, -1, 1, 1,
0, 1, 1, 0, 1, 1, -1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, -1, 1, 1,
0, 1, 1, 0, 1, 1, -1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, -1, 1, 1,
0, 1, 1, 0, 1, 1, -1, 1, 1};
static const char interval_index_up[]={1, 1, 0, 1, 1, -1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, -1, 1, 1, 0,
1, 1, 0, 1, 1, -1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, -1, 1, 1, 0,
1, 1, 0, 1, 1, -1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, -1, 1, 1, 0,
1, 1, 0, 1, 1, -1, 1, 1, 0};


note_position NoteOperations::getSmallSecunde(const note_position &base, bool up)
{
	note_position rez;
	rez=notes[getInterval(base.id,1,2,up)];
	return rez;
}


note_position NoteOperations::getBigSecunde(const note_position &base, bool up)
{
	note_position rez;
	rez=notes[getInterval(base.id,2,2,up)];
	return rez;
}

note_position NoteOperations::getSmallTerz(const note_position &base, bool up)
{
	note_position rez;
	rez=notes[getInterval(base.id,3,3,up)];
	return rez;
}

note_position NoteOperations::getBigTerz(const note_position &base, bool up)
{
	note_position rez;
	rez=notes[getInterval(base.id,4,3,up)];
	return rez;
}

char NoteOperations::calcStage(char base_id, char generate_id)
{
	return static_cast<char>(std::abs(((generate_id+2)/3)-((base_id+2)/3)));
}

char NoteOperations::getInterval(char base_id, char tone_sum, char stage_num, bool up)
{
	char sum=0;
	char i=base_id-1;
	char col_stage=0;
	while((sum!=tone_sum)||(col_stage<(stage_num-1)))
	{
		if((i<0)||(i>note_col))
		{
			throw std::out_of_range("Interval not create");
		}
		if(!up)
		{
			++i;
			sum+=interval_index_down[i];
		}
		else
		{
			--i;
			sum+=interval_index_up[i];
		}
		col_stage=calcStage(base_id,notes[i].id);
	}
	if((i>=note_col)||(i<0))
	{
		throw std::out_of_range("Interval not create");
	}
	if(calcStage(base_id,notes[i].id)==(stage_num-1))
	{
		return i;
	}
	else
	{
		throw std::invalid_argument("Interval not create");
	}

}

note_position NoteOperations::reductionNote(note_position base)
{
	switch(base.alt)
	{
		case 2:
		{
			base.alt=1;
			break;
		}
		case 1:
		{
			base.alt=0;
			break;
		}
		case 8:
		case 0:
		{
			base.alt=-1;
			break;
		}
		case -1:
		{
			base.alt=-2;
			break;
		}
	}
	return base;
}

note_position NoteOperations::increaseNote(note_position base)
{
	switch(base.alt)
	{
		case 1:
		{
			base.alt=2;
			break;
		}
		case 8:
		case 0:
		{
			base.alt=1;
			break;
		}
		case -1:
		{
			base.alt=0;
			break;
		}
		case -2:
		{
			base.alt=-1;
			break;
		}
	}
	return base;
}

note_position NoteOperations::getNotePosition(char index)
{
	return notes[index];
}

void NoteOperations::convertToClef(std::list<note_position> &in, int clef,int prevClef)
{
	int delta = -1;
	if (prevClef == 0)
	{
		switch (clef)
		{
			case 1:
			{
				delta=11;
				break;
			}
			case 2:
			{
				delta=17;
				break;
			}
			case 3:
			{
				delta=14;
				break;
			}
		}
	}
	if (prevClef == 1)
	{
		switch (clef)
		{
			case 0:
			{
				delta = -13;
				break;
			}
			case 2:
			{
				delta = 5;
				break;
			}
			case 3:
			{
				delta = 2;
				break;
			}
		}
	}
	if (prevClef == 2)
	{
		switch (clef)
		{
			case 0:
			{
				delta = -19;
				break;
			}
			case 1:
			{
				delta = -7;
				break;
			}
			case 3:
			{
				delta = -4;
				break;
			}
		}
	}
	if (prevClef ==3)
	{
		switch (clef)
		{
			case 0:
			{
				delta = -16;
				break;
			}
			case 1:
			{
				delta = -4;
				break;
			}
			case 2:
			{
				delta = 2;
				break;
			}
		}
	}
	for (note_position &val: in)
	{
		char alt = val.alt;
		char transp = 0;
		char tdelta = delta;
		if(val.id >0)
		{
			if ((val.id+tdelta) < 0)
			{
				tdelta += OCTAVE_OFFSET;
				transp = 1;
			}
			else
			{
				if ((val.id+tdelta) >= note_col)
				{
					tdelta -= (OCTAVE_OFFSET);
					transp = -1;
				}
			}
			val = notes[val.id+tdelta];
			val.alt = alt;
			val.transposition = transp;
		}
	}
}
