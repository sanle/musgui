#include "metronome.h"

MetronomePanel::MetronomePanel(wxFrame *parent):
	wxPanel(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxBORDER_NONE|wxVSCROLL)
{
	mainSizer = new wxBoxSizer(wxVERTICAL);
	wxButton *go = new wxButton(this,101,wxT("Start/Stop"));
	spinner = new wxSpinCtrl(this,102);
	spinner->SetRange(1,300);
	spinner->SetBase(40);
	spinSizer = new wxBoxSizer(wxHORISONTASL);
	spinSizer->Add(spinner,0,wxEXPAND|wxALL,0);
	spinSizer->Add(go,0,wxEXPAND|wxALL,0);
	mainSizer->Add(spinSizer,0,wxEXPAND|wxALL,0);
	this->SetSizer(mainSizer);
	timer = wxTimer(this,100);
	Connect(wxEVT_TIMER,wxTimerEventHandler(MetronomePanel::onTimer)); 
	Connect(wxEVT_SPIN,wxSpinEventHandler(MetronomePanel::onSpin)); 
}

void MetronomePanel::onTimer(wxTimerEvent& event)
{
}
