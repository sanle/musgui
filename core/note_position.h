#ifndef NOTE_POSITION_H
#define NOTE_POSITION_H
struct note_position
{
	char id; // note id
	char y; // Y position on staff 
	char additive_line; // number and direction additive line 
	char line_y; // start Y position for first additive line 
	char alt; // accidental
	char strich; // note strich 
	char art; // Articulation
	char transposition;
	char dots; // note value modifiers 
	char value; // note value

	bool operator== (const note_position &rhc) const
	{
		return ((this->id == rhc.id)&&(this->alt == rhc.alt)&&(this->value == rhc.value)); 
	}
};
#endif
