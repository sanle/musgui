#ifndef CIRCLE_LIST_H
#define CIRCLE_LIST_H

#include <iterator>
#include <stdexcept>

template <class T>
class CircularList
{
	private:
		class Node
		{
			public:
				T data;
				Node *next;
				Node *prev;
				Node(T element);
				Node();
		};
		Node *current;
		Node *first;

	public:
		void Add(T element);
		void RotateLeft();
		void RotateRight();
		CircularList();
		~CircularList();
		CircularList(T element);
		CircularList(const CircularList & obj);
		void deleteCurrent();
		T getCurrent() const;
		class iterator : public std::iterator<std::bidirectional_iterator_tag,Node>
		{
			private:
				Node *pt;
			public:
				iterator();
				iterator(Node * pn);
				T operator*();
				iterator& operator++();
				iterator operator++(int);
				iterator& operator--();
				iterator operator--(int);
				bool operator!=(const iterator &rhs) const;
		};
		iterator getFirst();
		iterator getCurrentIterator();
};

template <class T>
CircularList<T>::CircularList()
{
	first=nullptr;
	current=nullptr;
}
template <class T>
CircularList<T>::CircularList(T element)
{
	first= new Node(element);
	current=first;
	current->next=current;
	current->prev=current;
};

template <class T>
CircularList<T>::CircularList(const CircularList & obj):CircularList()
{
	Node *temp=obj.first;
	Node *ourcurrent;
	Add(temp->data);
	ourcurrent=first;
	temp=temp->next;
	while(temp!=obj.first)
	{
		if(temp==obj.current)
		{
			ourcurrent=current;
		}
		Add(temp->data);
		temp=temp->next;
	}
	current=ourcurrent;
}

template <class T>
CircularList<T>::~CircularList()
{
	if (current == nullptr) 
	{
		return;
	}
	if (first == current)
	{
		delete first;
		return;
	}
	first->prev->next=nullptr;
	while(first->next!=nullptr)
	{
		current=first;
		first=first->next;
		delete current;
	}
	delete first;
}

template <class T>
CircularList<T>::iterator::iterator()
{
	pt=nullptr;
}

template <class T> 
CircularList<T>::iterator::iterator(Node * pn)
{
	pt=pn;
}

template <class T>
T CircularList<T>::iterator::operator*()
{
	return pt->data;	
}

template <class T>
typename CircularList<T>::iterator& CircularList<T>::iterator::operator++()
{
	pt=pt->next;
	return *this;
}

template <class T>
typename CircularList<T>::iterator CircularList<T>::iterator::operator++(int)
{
	iterator temp = *this;
	pt=pt->next;
	return temp;
}

template <class T>
typename CircularList<T>::iterator& CircularList<T>::iterator::operator--()
{
	pt=pt->prev;
	return *this;
}

template <class T>
typename CircularList<T>::iterator CircularList<T>::iterator::operator--(int)
{
	iterator temp = *this;
	pt=pt->prev;
	return temp;
}

template <class T>
bool CircularList<T>::iterator::operator!=(const iterator &rhs) const
{
	return (this->pt->data!=rhs.pt->data);
}

template <class T>
CircularList<T>::Node::Node(T element)
{
	data=element;
	next=this;
	prev=this;
};

template <class T>
CircularList<T>::Node::Node()
{
	next=nullptr;
	prev=nullptr;
};

template <class T>
void CircularList<T>::Add(T element)
{
	if(first!=nullptr)
	{
		Node *tmp;
		tmp = current;
		current= new Node(element);
		current->next=first;
		current->prev=tmp;
		tmp->next=current;
		first->prev=current;
	}
	else
	{
		first= new Node(element);
		current=first;
		current->next=current;
		current->prev=current;
	}
};

template <class T>
void CircularList<T>::RotateLeft()
{
	if(current!=nullptr)
	{
		current=current->prev;
	}
}

template <class T>
void CircularList<T>::RotateRight()
{
	if(current!=nullptr)
	{
		current=current->next;
	}
}

template <class T>
void CircularList<T>::deleteCurrent()
{
	if (current!=nullptr)
	{
		if (current == current->next)
		{
			delete first;
			first = nullptr;
			current = nullptr;
			return;
		}
		if (current == first)
		{
			first = current->next;
		}
		Node * tcurrent= current->next;
		current->prev->next=current->next;
		current->next->prev=current->prev;
		delete current;
		current = tcurrent;
	}
}

template <class T>
T CircularList<T>::getCurrent() const
{
	if (current != nullptr) 
	{
		return current->data;
	}
	throw std::out_of_range("List is empty");
}

template <class T>
typename CircularList<T>::iterator CircularList<T>::getFirst()
{
	return iterator(first);
}

template <class T>
typename CircularList<T>::iterator CircularList<T>::getCurrentIterator()
{
	return iterator(current);
}
#endif
