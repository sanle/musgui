#ifndef MUSIC_SCALE_H
#define MUSIC_SCALE_H

#include <list>
#include "note_position.h"

extern note_position notes[72];

class MusicScale
{
	public:
		MusicScale();
		std::list<note_position> getDiatonic() const;
		std::list<note_position> getChromatic() const;
		std::list<note_position> getMajorPentatonic(note_position base) const;
		std::list<note_position> getMinorPentatonic(note_position base) const;
		std::list<note_position> getNaturalMajor(note_position base, bool arp = false, char col =1) const;
		std::list<note_position> getHarmonicMajor(note_position base,  char col =1) const;
		std::list<note_position> getMelodicMajor(note_position base,  char col =1) const;
		std::list<note_position> getNaturalMinor(note_position base, bool arp = false,  char col =1) const;	
		std::list<note_position> getHarmonicMinor(note_position base,  char col =1) const;
		std::list<note_position> getMelodicMinor(note_position base,  char col =1) const;
		void setClef(int clef);
		private:
		std::list<note_position> scale;
		static const char note_col = 72;
		int currentClef = 0;
};
#endif
