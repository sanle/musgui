#include "tonality.h"

Tonality::Tonality()
{
	nameMajor = "C";
	nameMinor = "a";
	fullNameMajor = "DO";
	fullNameMinor = "LA";
	alt=0;
	col_alt=0;
	majorIndex =46;
	minorIndex=52;
}

Tonality::Tonality(const std::string& NameMajor, const std::string& NameMinor, const std::string& FullNameMajor, const std::string& FullNameMinor, char Alt, char col, char Imajor, char Iminor)
{
	nameMajor = NameMajor;
	nameMinor = NameMinor;
	fullNameMajor = FullNameMajor;
	fullNameMinor = FullNameMinor;
	alt=Alt;
	col_alt=col;
	majorIndex=Imajor;
	minorIndex=Iminor;
}

std::string Tonality::getNameMajor() const
{
	return nameMajor;
}

std::string Tonality::getNameMinor() const
{
	return nameMinor;
}

std::string Tonality::getFullNameMajor() const
{
	return fullNameMajor;
}
std::string Tonality::getFullNameMinor() const
{
	return fullNameMinor;
}

char Tonality::getAlt() const
{
	return alt;
}

char Tonality::getColAlt() const
{
	return col_alt;
}

char Tonality::getMajorIndex() const
{
	return majorIndex;
}

char Tonality::getMinorIndex() const
{
	return minorIndex;
}

bool Tonality::operator!=(const Tonality &rhs) const
{
	return (this->getMinorIndex() != rhs.getMinorIndex());
}
