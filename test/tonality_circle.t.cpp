
#include "../core/tonality_circle.h"
#include "catch.hpp"

TEST_CASE("Tonality_circle class test","[tonality_circle][tonality][circular_list]")
{
	SECTION ("Constructor and getters")
	{
		TonalityCircle tc = TonalityCircle();
		REQUIRE(tc.getMajor() == "до-диез");
		REQUIRE(tc.getMinor() == "ля-диез");
		REQUIRE(tc.getMajorIndex() == 45);
		REQUIRE(tc.getMinorIndex() == 51);
		Tonality t = tc.getElement();
		Tonality t1 = Tonality("Cis","ais","до-диез","ля-диез",1,7,45,51);
		REQUIRE(t.getNameMajor() == t1.getNameMajor());
	}
	SECTION ("Rotation")
	{
		TonalityCircle tc = TonalityCircle();
		tc.rotateLeft();
		REQUIRE(tc.getMajor() == "фа-диез");
		tc.rotateRight();
		REQUIRE(tc.getMajor() == "до-диез");
	}
	SECTION ("Iterators")
	{
		TonalityCircle tc = TonalityCircle();
		CircularList<Tonality>::iterator it = tc.getIterator();
		REQUIRE((*it).getNameMajor() == "Cis");
		++it;
		REQUIRE((*it).getNameMajor() == "es");

	}
	SECTION ("Find")
	{
		TonalityCircle tc = TonalityCircle();
		REQUIRE(tc.getIndexByMajorName("") == -1);
		REQUIRE(tc.getIndexByMinorName("") == -1);
		REQUIRE(tc.getIndexByMajorName("до-диез") == 45);
		REQUIRE(tc.getIndexByMinorName("ля-диез") == 51);
		REQUIRE(tc.getIndexByMajorName("до") == 46);
		REQUIRE(tc.getIndexByMinorName("соль") == 55);
	}
}
